from django.db import models
from uuid import uuid1
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save, pre_save, m2m_changed
from django.dispatch import receiver
from .connector import create_project, edit_project, create_task, edit_task


class AsanaUser(models.Model):
    uuid = models.CharField(db_index=True, default=uuid1, max_length=255)
    gid = models.BigIntegerField(default=0, verbose_name=_('User Global ID'))
    email = models.CharField(max_length=255, verbose_name=_('E-mail'))
    name = models.CharField(max_length=255,  verbose_name=_('Имя'))
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, verbose_name=_('Создано'))

    class Meta:
        verbose_name = _('Asana Пользователь')
        verbose_name_plural = _('Asana Пользователи')

    def __str__(self):
        return self.email


class Project(models.Model):
    uuid = models.CharField(db_index=True, default=uuid1, max_length=255)
    workspace_gid = models.BigIntegerField(default=0, verbose_name=_('Workspace Global ID'))
    gid = models.BigIntegerField(default=0, verbose_name=_('Project Global ID'))
    name = models.CharField(max_length=255,  verbose_name=_('Имя проекта'))
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, verbose_name=_('Создано'))

    class Meta:
        verbose_name = _('Проект')
        verbose_name_plural = _('Проекты')

    def __str__(self):
        return self.name


class Task(models.Model):
    uuid = models.CharField(db_index=True, default=uuid1, max_length=255)
    gid = models.BigIntegerField(default=0, verbose_name=_('Task Global ID'))
    name = models.CharField(max_length=255, verbose_name=_('Название задачи'))
    text = models.TextField(verbose_name=_('Текст задачи'))
    assignee = models.ForeignKey(
        AsanaUser, related_name='user_tasks', on_delete=models.CASCADE, verbose_name=_('Исполнитель'))
    project = models.ForeignKey(
        Project, related_name='tasks', on_delete=models.CASCADE, verbose_name=_('Проект'))
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True, verbose_name=_('Создано'))

    class Meta:
        verbose_name = _('Задача')
        verbose_name_plural = _('Задачи')

    def __str__(self):
        return self.text


@receiver(post_save, sender=Project)
def project_post_save(sender, instance, **kwargs):
    if instance.gid == 0:
        project = create_project(
            workspace_id=instance.workspace_gid,
            name=instance.name
        )
        instance.gid = project['data']['gid']
        instance.save()
    else:
        edit_project(instance.gid, instance.workspace_gid, instance.name)


@receiver(post_save, sender=Task)
def task_post_save(sender, instance, **kwargs):
    if instance.gid == 0:
        task = create_task(
            project_id=instance.project.gid,
            assignee=instance.assignee.gid,
            name=instance.name,
            text=instance.text,
        )
        instance.gid = task['data']['gid']
        instance.save()
    else:
        edit_task(
            task_id=instance.gid,
            name=instance.name,
            text=instance.text,
            assignee=instance.assignee.gid
        )

