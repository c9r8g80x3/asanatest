import json
import requests
from django.http import HttpResponse
from django.shortcuts import render

from django.conf import settings

headers = {
    'Authorization': 'Bearer {token}'.format(token=settings.ASANA_API_KEY),
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}


def get_workspaces():
    url = 'https://app.asana.com/api/1.0/workspaces'
    r = requests.get(url, headers=headers)

    return json.loads(r.text)


def create_project(workspace_id, name):
    payload = {"data": {"name": name, "workspace": str(workspace_id)}}

    r = requests.post(
        'https://app.asana.com/api/1.0/projects',
        headers=headers, json=payload)

    return json.loads(r.text)


def edit_project(gid, workspace_id, name):
    payload = {"data": {"name": name, "workspace": str(workspace_id)}}

    r = requests.put(
        'https://app.asana.com/api/1.0/projects/{project_gid}'.format(project_gid=gid),
        headers=headers, json=payload)

    return json.loads(r.text)


def create_task(project_id, name, text, assignee):
    payload = {
        "data": {
            "projects": [
                str(project_id)
            ],
            "name": name,
            "notes": text,
            "assignee": str(assignee)
        }
    }

    r = requests.post(
        'https://app.asana.com/api/1.0/tasks',
        headers=headers, json=payload)
    print(r)

    return json.loads(r.text)


def edit_task(task_id, name, text, assignee):
    payload = {
        "data": {
            "name": name,
            "notes": text,
            "assignee": str(assignee)
        }
    }

    r = requests.put(
        'https://app.asana.com/api/1.0/tasks/{task_gid}'.format(task_gid=task_id),
        headers=headers, json=payload)
    print(r)

    return json.loads(r.text)


def get_user(email):
    url = 'https://app.asana.com/api/1.0/users/{user_gid}'.format(user_gid=email)
    r = requests.get(url, headers=headers)
    print(r.text)

    return json.loads(r.text)



