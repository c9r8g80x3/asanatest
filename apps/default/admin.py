from django.contrib import admin
from .models import AsanaUser, Project, Task
from .forms import ProjectAdminForm, UserAdminForm


@admin.register(AsanaUser)
class AsanaUserAdmin(admin.ModelAdmin):
    form = UserAdminForm
    list_display = ('name', 'email', 'created_at')
    fields = ['email']
    search_fields = ('email',)

    def has_change_permission(self, request, obj=None):
        if obj is not None:
            return False
        return super().has_change_permission(request, obj=obj)

    def save_model(self, request, obj, form, change):
        obj.gid = form.cleaned_data['gid']
        obj.name = form.cleaned_data['name']
        obj.save()


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    form = ProjectAdminForm


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    fields = (
        ('project', 'assignee'),
        'name', 'text',
    )
    autocomplete_fields = ('assignee',)
    list_display = ('project', 'name', 'text', 'assignee', 'created_at')
