from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from .connector import get_workspaces, get_user
from .models import AsanaUser, Project, Task


class ProjectAdminForm(forms.ModelForm):
    workspaces_list = get_workspaces()
    workspaces = []

    for workspace in workspaces_list['data']:
        workspaces.append((workspace['gid'], workspace['name']))

    workspace_gid = forms.ChoiceField(widget=forms.Select, choices=workspaces)

    class Meta:
        model = Project
        exclude = ['gid', 'uuid']


class UserAdminForm(forms.ModelForm):
    class Meta:
        model = AsanaUser
        exclude = ['uuid']

    def clean(self):
        email = self.cleaned_data['email']
        validate_email(email)

        user = get_user(email)
        if 'errors' in user:
            raise ValidationError(user['errors'][0]['message'], code='invalid')
        elif AsanaUser.objects.filter(email=email).exists():
            raise ValidationError('Already in exists', code='invalid')
        else:
            self.cleaned_data['name'] = user['data']['name']
            self.cleaned_data['gid'] = user['data']['gid']
