from django.http import HttpResponse
from django.conf import settings
import requests
import json

headers = {
    'Authorization': 'Bearer {token}'.format(token=settings.ASANA_API_KEY),
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}


def index(request):
    url = 'https://app.asana.com/api/1.0/users/{user_gid}'.format(user_gid='m.turaev152@gmail.com')
    r = requests.get(url, headers=headers)
    print('Workspaces: ', r.text)

    return HttpResponse('Hello world')

