После клонирования проекта требуется ввести(API) ключ от ASANA в 
в файл .env.dev

Далее выполнить команды.
1. docker-compose build
2. docker-compose up -d --build

Для создания superuser для Django
3. docker-compose exec web python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'adminpass')"
